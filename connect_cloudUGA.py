#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Mar 14 15:53:02 2022

@author: fred
""" 
import re
import os
import getpass

def connect_cloudUGA () : 
    NEXTCLOUD_URL = "http://cloud.univ-grenoble-alpes.fr"
    NEXTCLOUD_USERNAME = os.environ['LOGNAME']
    NEXTCLOUD_PASSWORD = getpass.getpass(stream=None)

    cloud = nextcloud_client.Client(NEXTCLOUD_URL)

    cloud.login(NEXTCLOUD_USERNAME, NEXTCLOUD_PASSWORD)
    
    return cloud

def return_long_url_file (cloud, short_url ) :
    URL = cloud.share_file_with_link(short_url)
    response = requests.get(URL.get_link())
    url = re.search('href=\"(.*)\" class', response.text).group(1)
    return url
    