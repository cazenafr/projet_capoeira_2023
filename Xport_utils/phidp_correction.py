#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Mar 14 11:37:49 2022

@author: davy
"""

import numpy as np
from functools import partial
import pyart
from pyart.config import get_fillvalue, get_field_name, get_metadata

def nse(targets,predictions):
    return 1-(np.sum((targets-predictions)**2)/np.sum((targets-np.mean(targets))**2))


def cor_phidp(radar, seuil_nval_phidp = 10, unfolded = False, parallel=False, phidp_field= 'differential_phase',rhv_field = 'cross_correlation_ratio'):
    
#    phidp_field = 'differential_phase'
#    rhv_field = 'cross_correlation_ratio'
    fill_value = -9999
    
    # create parallel computing instance
    if parallel:
        import multiprocessing as mp
        pool = mp.Pool(processes=mp.cpu_count(), maxtasksperchild=1)

    if unfolded == True :
        phidp = 180 - radar.fields[phidp_field]['data']
    else :
        phidp = radar.fields[phidp_field]['data']
  
    rhv = radar.fields[rhv_field]['data']
    
    phidp_cor = np.zeros(phidp.shape, dtype='float32')
    
    nsweeps = int(radar.nsweeps)
    
    for sweep in range(nsweeps):
        
        start_ray = radar.sweep_start_ray_index['data'][sweep]
        end_ray = radar.sweep_end_ray_index['data'][sweep] + 1
                
        all_phidp = list(phidp)
        all_rhohv = list(rhv)
        
        func = partial(compute_cor_phi, start_ray=start_ray,end_ray=end_ray,seuil_nval_phidp=seuil_nval_phidp)

        
        if parallel:
            list_phi_cor = pool.starmap(func,[all_phidp,all_rhohv])
        else:
            list_phi_cor = map(func, all_phidp,all_rhohv)
        
        all_phidp = list(phidp)
        all_rhohv = list(rhv)
        
        if parallel:
            list_phi_cor = pool.starmap(func,[all_phidp,all_rhohv])
        else:
            list_phi_cor = map(func, all_phidp,all_rhohv)

        compute_cor_phi(phidp[7,:],rhv[7,:],start_ray=start_ray,
                                 end_ray=end_ray,seuil_nval_phidp=seuil_nval_phidp)
            
    cor_phi = np.zeros(phidp.shape) * np.nan
    cor_phi = np.ma.masked_array(cor_phi, fill_value=fill_value)
    
    for i, l in enumerate(list_phi_cor):
        cor_phi[i, 0:len(l)] = l
        
    # Mask the reconstructed Phidp with the mask of original phidp
    if isinstance(phidp, np.ma.masked_array):
        masked = phidp.mask
        cor_phi = np.ma.array(cor_phi, mask=masked, fill_value=fill_value)

    # create reconstructed differential phase field dictionary and store data
    phidp_cor_dict = get_metadata(phidp_field)
    phidp_cor_dict['data'] = cor_phi
    
    if parallel:
        pool.close()

    return phidp_cor_dict

def compute_cor_phi(ray_phidp,ray_rhv,start_ray,end_ray,seuil_nval_phidp) :
    
#    for i in range(1,len(ray_phidp)):
#        if ray_rhv[i] < 0.95 :
#            ray_phidp[i]=ray_phidp[i-1]
    
    diffmax = np.arange(0.2,10,0.2)
    niter = len(diffmax)
        
    ideb = 6
    ifin = len(ray_rhv) # Correction  
    for j in range(len(ray_rhv)-10):
        if ray_rhv[j] < 0.95:
            ideb = j+1
        elif (j-ideb >= 10) :
                break
                
    for j in range(len(ray_rhv) - 1, 10, -1):
        if (ray_rhv[j] < 0.95):
            ifin = j-1
        # elif ifin-j >= 10:
        else:
            break
    if(ifin-ideb < 10):
        return ray_phidp * 0
    
    
    nport = len(ray_phidp)
    
    phidp0 = np.median(ray_phidp[ideb:ideb+5])
                
    phidpraw = np.zeros(nport)
    phidpraw[ideb:ifin] = ray_phidp[ideb:ifin] - phidp0
    phidpraw = np.where(phidpraw < 0, 0, phidpraw)
    
    
    
    # print("az = ", radar.azimuth['data'][i])
    # print(nport, ideb, ifin)
    # print("phidp0 = ", phidp0)
    
    
    
    #Nash = np.zeros(niter)
    Nash = np.full(niter, np.nan)
    vmin = np.zeros([nport, niter])
    vmax = np.zeros([nport, niter])
    vmoy = np.full([nport, niter], np.nan)
    
                    
    # Boucle sur iter
    #================
    for iter in range(niter):
                    
      
        # calcul de vmax
        #---------------
        vmax[1:(ideb-1),iter] = 0
        
        for ind1 in range(ideb+1,ifin):
            diff = phidpraw[ind1] - vmax[ind1-1,iter]
            vmax[ind1,iter] = vmax[(ind1-1),iter]
            if(diff>0 and diff<=diffmax[iter]): vmax[ind1,iter] = phidpraw[ind1]
            elif  (diff>diffmax[iter]) : vmax[ind1,iter] = vmax[(ind1-1),iter] + diffmax[iter]
            else : vmax[ind1,iter] = vmax[(ind1-1),iter]
            if (np.isnan(phidpraw[ind1])):
                vmax[ind1,iter] = vmax[(ind1-1),iter]
        
        
        # calcul de vmin
        #---------------
        # recherche de la premiere valeur positive en partant de la fin
        ind6 = np.nan
        for ind5 in range(ifin,ideb) :
            if (phidpraw[ind5] > 0) and not np.isnan(phidpraw[ind5]):
                ind6 = ind5
                vmin[ind6,iter] = phidpraw[ind6]
                break
          
    
        # calcul vmoy si ind6 existe
        #---------------------------
        if(not np.isnan(ind6)):
            for ind2 in range((ind6-1), ideb):
                diff = vmin[(ind2+1),iter] - phidpraw[ind2]
                if(diff > 0 & diff < diffmax[iter]) : vmin[ind2,iter] = phidpraw[ind2]
                elif (diff>diffmax[iter]): vmin[ind2,iter] = vmin[(ind2+1),iter]-diffmax[iter]
                else : vmin[ind2,iter] = vmin[(ind2+1),iter]
                
                if (np.isnan(phidpraw[ind2])) : vmin[ind2,iter] = vmin[(ind2+1),iter]
                if (vmin[ind2,iter] > vmax[ind2,iter]) : vmin[ind2,iter] = vmax[ind2,iter]
            
            
            if(ind6 < nport):
                vmin[ind6:nport,iter] = np.nan
                vmax[ind6:nport,iter] = np.nan
          
        vmoy[:,iter] = (vmin[:,iter] + vmax[:,iter])
      
        # print(paste(" iter, vmin : ",iter,sep=""))
        # print(paste(as.numeric(vmin[1:100,iter])))    
        # print(paste(" iter, vmoy : ",iter,sep=""))
        # print(paste(as.numeric(vmoy[1:100,iter])))  
        # print(paste(" iter, vmax : ",iter,sep=""))
        # print(paste(as.numeric(vmax[1:100,iter])))  
      
      
        # calcul MAD entre phidpraw et vmoy
        #----------------------------------
        good = np.logical_and(np.logical_not(np.isnan(phidpraw)), np.logical_not(np.isnan(vmoy[:,iter])))
        
                        
        y = np.extract(good, vmoy[:,iter])
        x = np.extract(good, phidpraw)
        
        nval = len(x)
      
        #Nash[iter] = np.nan
        if(nval >= seuil_nval_phidp):
            Nash[iter] =  nse(x,y)
            # mean_observed = sum(x) / nval
            # numerator = 0
            # denominator = 0                    
            # for j in range(nval):
            #     numerator += (x[j] - y[j])*(x[j] - y[j])
            #     denominator += (x[j] - mean_observed)*(x[j] - mean_observed)
            # Nash[iter] = 1-(numerator/denominator)
      
        # print(paste(" iter, Nash[iter] : ",iter, Nash[iter],sep=" "))
        
        # fin de boucle sur iter
        
        NashnoNA = np.extract(np.logical_not(np.isnan(Nash)), Nash)
        
        # au moins un Nash non NA   
        if (len(NashnoNA) >= 1):
        
            ibest = np.nanargmax(Nash)
            
            Nash_opt = Nash[ibest]
            
            # print(" ibest : ",ibest," Nash :",Nash_opt)
            
            # vmin_ret = vmin[:,ibest]
            # vmax_ret = vmax[:,ibest]
            # vmoy_ret = vmoy[:,ibest]
            
                
            return vmoy[:,ibest]   
        else :
            #print ("No Nash")
            return ray_phidp * 0